package services;

import dao.FlightsDAO;
import entities.Cities;
import entities.Flights;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightsServicesTest {
    FlightsServices flightsServices;
    FlightsDAO flightsDAO;
    File file;
    Flights flights;
    List<Flights> flightsListToWrite;
    List<Flights> flightsListToRead;

    @BeforeEach
    void beforeTest() {
        flightsDAO = new FlightsDAO("flightsTest.txt");
        flights = new Flights(22, "Kiev", Cities.Moscow, "28-03-2020 14:49 ", 179);
        flightsListToWrite = new ArrayList<>();
        flightsListToWrite.add(flights);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("flightsTest.txt")))) {
            oos.writeObject(flightsListToWrite);
        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
        flightsServices = new FlightsServices();

    }

    @Test
    void findByFrom() {
        flightsListToRead = (List<Flights>) flightsServices.findbyFrom("Moscow", "28-03-2020 14:49");
        assertEquals(flightsListToWrite, flightsListToRead);
    }

    @Test
    void getFlights() {
        flightsListToRead = (List<Flights>) flightsDAO.getAll();
        assertEquals(flightsListToRead, flightsServices.findbyFrom("Moscow", "28-03-2020 14:49"));
    }

    @Test
    void getFlightsById() {
        flightsListToRead = (List<Flights>) flightsDAO.getAll();
        assertEquals(flightsListToRead.get(0), flightsListToWrite.get(0));
    }
}