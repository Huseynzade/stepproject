package services;

import dao.BookingDAO;
import entities.Booking;
import entities.Cities;
import entities.Flights;
import entities.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingServiceTest {
    BookingService bookingService;
    BookingDAO bookingDAO;
    Booking booking;
    List<Booking> bookingToWrite;
    Passenger passenger;
    List<Passenger> passengers;
    List<Booking> bookingToRead;
    Flights flights;

    @BeforeEach
    void beforeTest() {
        bookingDAO = new BookingDAO("bookingTest.txt");
        passenger = new Passenger("Shaiq", "Huseynzade");
        passengers = new ArrayList<>();
        passengers.add(passenger);
        flights = new Flights(22, "Kiev", Cities.Moscow, "28-03-2020 14:49", 179);
        booking = new Booking(1, passengers, flights);
        bookingToWrite = new ArrayList<>();
        bookingToWrite.add(booking);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("bookingTest.txt")))) {
            oos.writeObject(bookingToWrite);

        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
        bookingService = new BookingService();
    }

    @Test
    void booking() {
        bookingToRead = (List<Booking>) bookingDAO.getAllBy(booking1 -> booking1.getFlights().getToCity().city().contains("Moscow"));
        assertEquals(bookingToRead, bookingToWrite);
    }

    @Test
    void getBooked() {
        bookingToRead = (List<Booking>) bookingDAO.getAll();
        assertEquals(bookingToWrite, bookingToRead);
    }

    @Test
    void deleteBooking() {
        bookingToRead=(List<Booking>) bookingDAO.getAll();
        assertEquals(bookingToRead.remove(0),bookingToWrite.remove(0));
    }
}