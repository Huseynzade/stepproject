package dao;

import entities.Cities;
import entities.Flights;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightsDAOTest {
    FlightsDAO flightsDAO;
    File file;
    Flights flights;
    List<Flights> flightsListToWrite;
    List<Flights> flightsListToRead;

    @BeforeEach
    void beforeTest() {
        flightsDAO = new FlightsDAO("flightsTest.txt");
        flights = new Flights(22, "Kiyev", Cities.Moscow, "28-03-2020 14:49", 179);
        flightsListToWrite = new ArrayList<>();
        flightsListToWrite.add(flights);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("flightsTest.txt")))) {
            oos.writeObject(flightsListToWrite);
        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }

    }

    @Test
    void getAll() {
        flightsListToRead = (List<Flights>) flightsDAO.getAll();
        System.out.println(flightsListToWrite);
        System.out.println(flightsListToRead);
        assertEquals(flightsListToWrite, flightsListToRead);
    }

    @Test
    void getAllBy() {
        flightsListToRead = (List<Flights>) flightsDAO.getAllBy(f -> f.getToCity().city().contains("Moscow"));
        System.out.println(flightsListToRead);
        assertEquals(flightsListToWrite, flightsListToRead);

    }

    @Test
    void createFile() {
        flightsDAO.create(flights);
        flightsListToRead=(List<Flights>) flightsDAO.getAll();
        System.out.println(flightsListToRead);
        flightsListToWrite.add(flights);
        System.out.println(flightsListToWrite);
        assertEquals(flightsListToRead,flightsListToWrite);

    }

    @Test
    void getById() {
        flightsListToRead = (List<Flights>) flightsDAO.getAll();
        assertEquals(flightsListToWrite.get(0), flightsListToRead.get(0));

    }

    @Test
    void delete() {
        flightsListToRead = (List<Flights>) flightsDAO.getAll();
        assertEquals(flightsListToWrite.remove(0), flightsListToRead.remove(0));
    }
}