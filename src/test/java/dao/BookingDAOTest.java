package dao;

import entities.Booking;
import entities.Cities;
import entities.Flights;
import entities.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class BookingDAOTest {
    BookingDAO bookingDAO;
    Booking booking;
    List<Booking> bookingToWrite;
    Passenger passenger;
    List<Passenger> passengers;
    List<Booking> bookingToRead;
    Flights flights;

    @BeforeEach
    void beforeTest() {
        bookingDAO = new BookingDAO("bookingTest.txt");
        passenger = new Passenger("Shaiq", "Huseynzade");
        passengers = new ArrayList<>();
        passengers.add(passenger);
        flights = new Flights(22, "Kiev", Cities.Moscow, "28-03-2020 14:49", 179);
        booking = new Booking(1, passengers, flights);
        bookingToWrite = new ArrayList<>();
        bookingToWrite.add(booking);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("bookingTest.txt")))) {
            oos.writeObject(bookingToWrite);

        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
    }

    @Test
    void getAll() {
        bookingToRead = (List<Booking>) bookingDAO.getAll();
        assertEquals(bookingToWrite, bookingToRead);
    }


    @Test
    void getAllBy() {
        bookingToRead=(List<Booking>)bookingDAO.getAllBy(booking1 -> booking1.getPassengers().get(0).name.contains("Shaiq"));
        assertEquals(bookingToRead,bookingToWrite);
    }

    @Test
    void get() {
        bookingToRead=(List<Booking>) bookingDAO.getAll();
        assertEquals(bookingToRead.get(0),bookingToWrite.get(0));
    }


    @Test
    void delete() {
        bookingToRead=(List<Booking>) bookingDAO.getAll();
        assertEquals(bookingToRead.remove(0),bookingToWrite.remove(0));
    }
}