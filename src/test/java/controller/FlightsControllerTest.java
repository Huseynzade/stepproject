package controller;

import dao.FlightsDAO;
import entities.Cities;
import entities.Flights;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.FlightsServices;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FlightsControllerTest {
    FlightsController flightsController;
    FlightsDAO flightsDAO;
    File file;
    Flights flights;
    List<Flights> flightsListToWrite;
    List<Flights> flightsListToRead;
    Optional<Flights> flightsListToRead2;
//    FlightsServices flightsServices;

    @BeforeEach
    void beforeTest() {
        flightsDAO = new FlightsDAO("flightsTest.txt");
        flights = new Flights(22, "Kiev", Cities.Moscow, "28-03-2020 14:49 ", 179);
        flightsListToWrite = new ArrayList<>();
        flightsListToWrite.add(flights);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("flightsTest.txt")))) {
            oos.writeObject(flightsListToWrite);
        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
        flightsController = new FlightsController(new FlightsServices());

    }

    @Test
    void getFControllerSearch() {
        flightsListToRead = (List<Flights>) flightsController.getFControllerSearch("Moscow", "28-03-2020 14:49");
//        System.out.println(flightsListToRead);
        assertEquals(flightsListToWrite, flightsListToRead);
    }

    @Test
    void getFlights() {
        flightsListToRead=(List<Flights>)flightsDAO.getAll();
        assertEquals(flightsListToRead,flightsController.getFControllerSearch("Moscow", "28-03-2020 14:49"));
    }

    @Test
    void getFlightById() {
        flightsListToRead2 =Optional.ofNullable(flights);
//        System.out.println(flightsListToWrite.get(0));
        assertEquals(flightsListToRead2.get(),flightsListToWrite.get(0));

    }


}