package controller;

import entities.Booking;
import services.BookingService;

import java.util.Collection;

public class BookingController {
    private BookingService bs;

    public BookingController(BookingService bs) {
        this.bs = bs;
    }

    public void booking(Booking booking) {
        bs.booking(booking);
    }

    public Collection<Booking> getBooking() {
       return bs.getBooked();
    }

    public void deleteBooking() {
        bs.deleteBooking();
    }
//    public Collection<Booking> getByName(){
//        return bs.getBookingByName();
//    }
}
