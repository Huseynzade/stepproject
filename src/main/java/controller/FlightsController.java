package controller;

import entities.Flights;
import services.FlightsServices;

import java.util.Collection;

public class FlightsController {
    private FlightsServices fs;


    public FlightsController(FlightsServices fs) {
        this.fs = fs;
    }

    public void generateFlights() {
        fs.generateFService();
    }
    public void getFlights(){

        fs.getFlights();
    }
    public void getFlightById(int n){
        fs.getFlightsById(n);
    }
    public Collection<Flights> getFControllerSearch(String to, String dateTime){
       return fs.findbyFrom(to,dateTime);
    }


}
