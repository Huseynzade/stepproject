package dao;

import entities.Passenger;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

public class PassengerDAO implements  DAO<Passenger> {


    @Override
    public Collection<Passenger> getAll() {
        return null;
    }

    @Override
    public Collection<Passenger> getAllBy(Predicate<Passenger> p) {
        return null;
    }

    @Override
    public Optional<Passenger> get(int id) {
        return Optional.empty();
    }

    @Override
    public void create(Passenger data) {

    }

    @Override
    public void delete(int id) {

    }
}
