package dao;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

public interface DAO <T>{
    Collection<T> getAll();
    Collection<T> getAllBy(Predicate<T> p);
    Optional<T> get(int id);
    void create(T data);
    void delete(int id);
}
