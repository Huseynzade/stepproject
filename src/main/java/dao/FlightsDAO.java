package dao;


import entities.Flights;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FlightsDAO implements DAO<Flights>{
    //    private static final List<Cities> cities = Arrays.asList(Cities.values());
//    private static final int size = cities.size();
//    private static final Random r = new Random();
    private File file;

    public FlightsDAO(String filename) {
        this.file = new File(filename);
    }

    @Override
    public Collection<Flights> getAll() {

        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object read = ois.readObject();
            List<Flights> objects = (ArrayList<Flights>) read;
            return objects;

        } catch (IOException | ClassNotFoundException ex) {
//            throw new RuntimeException("dao:getAll()", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public Collection<Flights> getAllBy(Predicate<Flights> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public Optional<Flights> get(int id) {
        return getAll().stream().filter(s -> s.id == id).findFirst();
    }

    @Override
    public void create(Flights data) {
        Collection<Flights> flights = getAll();
        flights.add(data);
        write(flights);
    }

    @Override
    public void delete(int id) {
        Collection<Flights> flights = getAllBy(s -> s.id != id);
        write(flights);
    }

    public void write(Collection<Flights> flights) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(flights);
        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
    }
}
