package dao;

import entities.Booking;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BookingDAO implements DAO<Booking> {
    private File file;

    public BookingDAO(String file) {
        this.file = new File(file);
    }
    @Override
    public Collection<Booking> getAll() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object read = ois.readObject();
            List<Booking> objects = (ArrayList<Booking>) read;
            return objects;
        } catch (IOException | ClassNotFoundException ex) {
//            throw new RuntimeException("dao:getAll()", ex);
            return new ArrayList<>();
        }
    }
//    public Optional<Booking> getUserList(List<Passenger> lu){
//        return getAll().stream().filter(booking -> booking.passengers==lu).findAny();
//    }

    @Override
    public Collection<Booking> getAllBy(Predicate<Booking> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public Optional<Booking> get(int id) {
        return getAll().stream().filter(booking -> booking.bookingId == id).findFirst();
    }


    @Override
    public void create(Booking data) {
        Collection<Booking> bookings = getAll();
        bookings.add(data);
        write(bookings);
    }

    @Override
    public void delete(int id) {
        Collection<Booking> flights = getAllBy(booking -> booking.bookingId != id);
        write(flights);
    }

    public void write(Collection<Booking> bookings) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(bookings);
//            oos.writeObject("\n");

        } catch (IOException ex) {
            throw new RuntimeException("dao:write:IOException", ex);
        }
    }
}
