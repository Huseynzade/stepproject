import controller.FlightsController;
import services.FlightsServices;
import util.ActionMenu;

public class Main {
    public static void main(String[] args) {
        ActionMenu x = new ActionMenu(new FlightsController(new FlightsServices()));
        x.showMenu();
    }
}

