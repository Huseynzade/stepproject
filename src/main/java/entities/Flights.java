package entities;

import java.io.Serializable;
import java.util.Objects;

public class Flights implements Serializable {
    private static final long serialVersionUID = 1L;
    public int id;
    private String time;
    private String fromCity;
    private Cities toCity;
    private int seats;
public Flights(){}
    public Flights(int id, String fromCity, Cities toCity, String time, int seats) {
        this.id = id;
        this.time = time;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.seats = seats;
    }
//
//    public Flights(int id, Cities toCity) {
//        this.id = id;
//        this.toCity = toCity;
//    }

    public int getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getFromCity() {
        return fromCity;
    }

    public Cities getToCity() {
        return toCity;
    }

    public int getSeats() {
        return seats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flights flights = (Flights) o;
        return id == flights.id &&
                seats == flights.seats &&
                Objects.equals(time, flights.time) &&
                Objects.equals(fromCity, flights.fromCity) &&
                toCity == flights.toCity;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, time, fromCity, toCity, seats);
    }


//    public static Flights parse(String line) {
//        String[] chunks = line.split(":");
//        return new Flights(
//                Integer.parseInt(chunks[0]),
//                chunks[1],
//                chunks[2],
//                chunks[3],
//                Integer.parseInt(chunks[4])
//        );
//    }

    public String represent() {
       return String.format("|%d|%10s|%5s|%10s|%10d|", id, time, fromCity, toCity, seats);
//        return String.format("%d:%s", new Flights(RandomFlights.randomID(),RandomFlights.randomCities()));

    }

    @Override
    public String toString() {
        return String.format("|%d|%10s|%5s|%10s|%10d|", id, time, fromCity, toCity, seats);
    }
}
