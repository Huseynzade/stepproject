package entities;

public enum Cities {
//    Baku,
//    Budapest,
//    Moscow,
//    Istanbul,
//    Paris,
//    Roma,
//    Madrid,
//    Barcelona,
//    NewYork,
//    Washington
//
    Baku("Baku"),
    Budapest("Budapest"),
    Moscow("Moscow"),
    Istanbul("Istanbul"),
    Paris("Paris"),
    Roma("Roma"),
    Madrid("Madrid"),
    Barcelona("Barcelona"),
    NewYork("NewYork"),
    Washington("Washington");


    private String city;

    private Cities(String city){
        this.city=city;
    }

    public  String city() {
        return city;
    }
}
