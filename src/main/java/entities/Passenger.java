package entities;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Passenger implements Serializable {
    private static final long serialVersionUID = 1L;
    private final AtomicInteger count = new AtomicInteger(0);
    private int userId;
    public String name;
    private String surname;

    public Passenger() {
    }

    public Passenger(String name, String surname) {
//        userId = count.incrementAndGet();
        this.name = name;
        this.surname = surname;
    }
    //    private int sets;

//    public int getSets() {
//        return sets;
//    }
//
//    public void setSets(int sets) {
//        this.sets = sets;
//    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String represent() {
        return String.format("%s:%s", name, surname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return userId == passenger.userId &&
//                Objects.equals(count, passenger.count) &&
                Objects.equals(name, passenger.name) &&
                Objects.equals(surname, passenger.surname);
    }

    @Override
    public int hashCode() {

        return Objects.hash( name, surname);
    }

    @Override
    public String toString() {
        return "Passenger{" +
//                "userId=" + userId +
                " Name='" + name + '\'' +
                " ; Surname='" + surname + '\'' +
                '}';
    }
}
