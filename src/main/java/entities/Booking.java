package entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Booking implements Serializable {
    private static final long serialVersionUID = 1L;
    public  int bookingId;
     public List<Passenger> passengers;
    private  Flights flights;
public Booking(){}
    public Booking(int bookingId, List<Passenger> passengers, Flights flights) {
    this.bookingId = bookingId;
        this.passengers = passengers;
        this.flights=flights;

    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }
//
//    public int getFlight_id() {
//        return flight_id;
//    }

    public Flights getFlights() {
        return flights;
    }
//    public void setFlight_id(int flight_id) {
//        this.flight_id = flight_id;
//    }
//
//    public int getUserId() {
//        return user_id;
//    }
//
//    public void setUserId(int user_id) {
//        this.user_id = user_id;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return bookingId == booking.bookingId &&
                Objects.equals(passengers, booking.passengers) &&
                Objects.equals(flights, booking.flights);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bookingId, passengers, flights);
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingId=" + bookingId +
                ", passengers=" + passengers+
                ", flights=" + flights +
                '}';
    }
}
