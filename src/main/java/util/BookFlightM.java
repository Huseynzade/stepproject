package util;

import controller.BookingController;
import controller.FlightsController;
import entities.Booking;
import io.Console;
import io.UnixConsole;
import services.BookingService;
import services.FlightsServices;

import java.util.Scanner;

public class BookFlightM {
    private BookingController bc = new BookingController(new BookingService());
    private FlightsController fc = new FlightsController(new FlightsServices()) ;
    Console console = new UnixConsole(new Scanner(System.in));
    public void bookFlightM(Booking booking){
        System.out.println("Enter where do you want to go : ");
        String from = console.readLnNext();
        System.out.println("Enter date and time too ");
        String dateTime = console.readLnNext();
        System.out.println(fc.getFControllerSearch(from, dateTime));
        System.out.println("------ Do you want to book , Enter YES or NO -------");
        String s = console.readLnNext();
        if (s.equalsIgnoreCase("Yes")) {
            System.out.println("------ How much tickets do you want : ------ ");
            int tickets = console.readInt();
            for(int i=0;i<tickets;i++) {
                bc.booking(booking);
                if(tickets>1){
                    System.out.println("-----FOR ANOTHER PERSON----- ");
                }
            }
            System.out.println("------BOOKED SUCCESSFULLY------");
        } else {
            return;
        }
    }
}
