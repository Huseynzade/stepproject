package util;

import controller.BookingController;
import controller.FlightsController;
import dao.BookingDAO;
import entities.Booking;
import entities.Passenger;
import services.BookingService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ActionMenu {
    private FlightsController fc;
    private BookingController bc = new BookingController(new BookingService());
    private GetFlightM fm = new GetFlightM();
    private BookFlightM bm = new BookFlightM();
    private Booking booking = new Booking();
    private GetBookedFlights gfb = new GetBookedFlights();

    public ActionMenu(FlightsController fc) {
        this.fc = fc;
    }

    public void showMenu() {
//        fc.generateFlights();

        while (true) {
            try {
                int menu = InputUtil.requireNumber("------MENU------" + "\n1. Online-board." + "\n2. Show the flight info." + "\n3. Search and book a flight." + "\n4. Cancel the booking." + "\n5. My flights" + "\n6. Exit " + "\nPlease , choose one of these points ");

                switch (menu) {
                    case 1:
                        fc.getFlights();
                        break;
                    case 2:
                        fm.getFlightM();
                        break;
                    case 3:
                        bm.bookFlightM(booking);
                        break;
                    case 4:
                        bc.deleteBooking();
                    case 5:
//                        bc.getBooking().forEach(System.out::println);
                        gfb.getBookedFlights();
                    case 6:
                        System.out.println("BYE");
                        break;
                }

                if (menu == 6) {
                    return;
                }
            } catch (InputMismatchException e) {
                System.out.println("Please ,input number if you want to choose from menu");
            }
        }

    }

}
