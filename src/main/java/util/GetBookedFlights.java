package util;

import controller.BookingController;
import entities.Booking;
import entities.Passenger;
import io.Console;
import io.UnixConsole;
import services.BookingService;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class GetBookedFlights {
    BookingController bookingController = new BookingController(new BookingService());
    Console console = new UnixConsole(new Scanner(System.in));
    public void getBookedFlights(){
        System.out.println("Enter name and surname ");
        while (true) {
            try {
                String[] name = console.readLn().trim().split(" ");
                Passenger p = new Passenger(name[0], name[1]);
                List<Booking> b1 = bookingController
                        .getBooking()
                        .stream()
                        .filter(b -> Objects.nonNull(b.getPassengers()))
                        .filter(b -> b
                                .getPassengers()
                                .stream()
                                .anyMatch(passenger -> passenger.getName().equals(name[0]) && passenger.getSurname().equals(name[1])))
                        .collect (Collectors.toList());
                b1.forEach(System.out::println);
                break;
            } catch (NullPointerException | ArrayIndexOutOfBoundsException n) {
                System.out.println("please try again");
            }
        }
    }
}
