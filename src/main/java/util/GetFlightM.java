package util;

import controller.FlightsController;
import io.Console;
import io.UnixConsole;
import services.FlightsServices;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GetFlightM {
    FlightsController fc = new FlightsController(new FlightsServices());
    Console console = new UnixConsole(new Scanner(System.in));

    public void getFlightM() {
        int n = 0;
        do {
            try {
                System.out.println("Enter id please : ");
                n = console.readInt();
            } catch (InputMismatchException e) {
                System.out.println("Please input a number");
            } ;
          console.readLn();
        } while (n == 0);
        fc.getFlightById(n);
    }
}
