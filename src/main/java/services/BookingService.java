package services;

import dao.DAO;
import dao.BookingDAO;
import entities.Booking;
import entities.Flights;
import dao.FlightsDAO;
import entities.Passenger;

import java.util.*;
import java.util.stream.Collectors;

public class BookingService {
    private DAO<Booking> bd = new BookingDAO("booking.txt");
    DAO<Flights> flightDAO = new FlightsDAO("flights.txt");
    Scanner in = new Scanner(System.in);

    public BookingService(DAO<Booking> bd) {
        this.bd = bd;
    }
    public BookingService(){}

    public void booking(Booking b) {
        System.out.println("Enter flight id :");
        int flight_id = in.nextInt();
        in.nextLine();
        System.out.println("Enter your name: ");
        String name = in.nextLine();
        System.out.println("Enter your surname :");
        String surname = in.nextLine();
        ArrayList<Passenger> ul = new ArrayList<>();
        ul.add(new Passenger(name, surname));
        b.setBookingId(bd.getAll().size() + 1);
        flightDAO.get(flight_id)
                .ifPresent(f -> {
                    bd.create(new Booking(b.getBookingId(), ul, f));
                });
    }

    public Collection<Booking> getBooked() {
        return bd.getAll();
    }

    public void deleteBooking() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter an ID of booking which you want to delete : ");
        int id = in.nextInt();
        bd.delete(id);
        System.out.println("------DELETED SUCCESSFULLY------");
    }
}
