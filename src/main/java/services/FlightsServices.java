package services;

import dao.DAO;
import dao.FlightsDAO;
import entities.Flights;
import generation.RandomFlights;

import java.util.Collection;
import java.util.function.Predicate;

public class FlightsServices {
    private RandomFlights rf = new RandomFlights();
    private DAO<Flights> fd = new FlightsDAO("flights.txt");


//    public FlightsServices(RandomFlights rf,FlightsDAO fd) {
//        this.rf = rf;
//        this.fd=fd;
//    }

    public void generateFService() {
        rf.generateFlights();
    }


    public Collection<Flights> findbyFrom( String to,String dateTime) {
//        int i = ;fd.getAllBy(flights -> flights.s)
         return fd.getAllBy(f -> f.getToCity().name().contains(to) && f.getTime().contains(dateTime));
    }

    public void getFlights() {
        System.out.format("|%s|%13s%6s%5s|%10s|%12s", "ID", "DATE&TIME", "|", "FROM", "TO", "SEATS|\n");
        fd.getAll().forEach(System.out::println);
    }

    public void getFlightsById(int n) {
        System.out.format("|%s|%13s%6s%5s|%10s|%12s", "ID", "DATE&TIME", "|", "FROM", "TO", "SEATS|\n");
        System.out.println(fd.get(n).map(Flights::represent).orElse("No flights found"));
    }
}
