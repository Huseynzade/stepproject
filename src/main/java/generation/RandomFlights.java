package generation;

import dao.FlightsDAO;
import entities.Cities;
import entities.Flights;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class RandomFlights {


    private static final List<Cities> cities = Arrays.asList(Cities.values());
    private static final int size = cities.size();
    private static final Random r = new Random();

    public void generateFlights() {
        FlightsDAO fd = new FlightsDAO("flights.txt");
        ArrayList<Flights> lf = new ArrayList<>();
        int id = 1;
        for (int i = 0; i <= 30; i++) {
            String fromCity = "Kiev";
            Cities toCity = cities.get(r.nextInt(size));
            String flightTime = LocalDateTime.now()
                    .plusDays((int) (Math.random() ))
                    .plusHours((int) (Math.random() *24))
                    .plusMinutes((int) (Math.random() * 60 + 1))
                    .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm "));
            int seats = (int) (Math.random() * 300 + 1);
           Flights f = new Flights(id++, fromCity, toCity, flightTime, seats);
           lf.add(f);
           fd.write(lf);
//           fd.create(f);
        }


    }

}
